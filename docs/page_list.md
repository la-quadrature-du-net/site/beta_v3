* Accueil
  * bloc thématique
  * focus sur (carrousel de : image + leins)
  * bloc campagne
  * bloc follow us
  * bloc liste rp
  * bloc dernieres actus + pagination
  * bloc newsletter
  * footer
    * têtes de ponts de chaque trucs
  * Actualités (Général)
    * Communiqués de presse
      * date de pub
      * chapeau
      * tags
      * dossier
      * thématique
      * contenu
      * liens/note de bas de page
      * illustration
      * external content
    * Vie de la Quadrature
      arrivés, départs, rapport annuels, vie associative
      * date de pub
      * auteur (faculattif)
      * chapeau
      * dossier
      * illus
      * contenu
      * tag
      * liens/notes de bas de page
      * thématique
      * external content
    * Actu du Garage
      * date de pub
      * auteur
      * date d'évenement (eventually)
      * contact (mailto)
      * chapeau
      * dossier
      * contenu
      * illustration
      * tags
      * thématique
      * external content
    * Blogs ?
      * date de pub
      * auteur (mandatory)
      * illus, chapeau, contenu, notes, liens
      * du même auteur
      * thématique
      * dossier
      * tags
      * external content
    * Thématiques
        * Infrastructure et réseaux
        * Droits fondamentaux
        * Société numérique
        * Droit d'auteur
          * nom
          * tags associés (dossier)
          * description
          * chapeau
          * articles liés
          * illustration
    * Nos propositions (En travaux, je ne sais pas s'il y aura des sous pages)
      * thématique
      * résumé
      * titre
      * texte
      * docs associés
      * tags
    * Ressources
      * PiPhone
      * Mediakit
      * Chat IRC
      * QuadPad
      * Mémoire Politique
      * …
        * nom
        * présentation
        * image
        * liens
    * Qui Sommes Nous
      * La Quadrature
        * texte
        * illustrations
        * liens internes
        * staff / ca
      * Revue de Presse
        * titre d'article
        * date de publi
        * date d'ajout
        * texte
        * illus
        * lien
        * tags
        * themes
      * Les soutiens
        * page classique
    * Contact
      * adresse
      * carte
      * formulaire de contact (nom, mail, sujet, body)
      * follow us on whatever
      * espace presse
      * abonnement NL
    * Participer / Soutenir (ou les deux dans la même page ?)
      * blocs de:
        * titre
        * texte
        * lien
        * form (optionnel)
    * Mentions Légales
      * texte + liens
    * FAQ
      * série de:
        * question
        * réponse (texte + liens)
    * Connexion contributeur
    * Résultats recherche
